import 'package:flutter/material.dart';

import 'model/char_users_model.dart';
import 'widgets/conversation_list.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  List<ChatUsers> chatUsers = [
    ChatUsers(name: "Robert Lewandowski", messageText: "Siemano", imageURL: "images/lewy.jpeg", time: "4 Cze"),
    ChatUsers(name: "Robert Lewandowski", messageText: "Siemano", imageURL: "images/lewy.jpeg", time: "4 Cze"),
    ChatUsers(name: "Robert Lewandowski", messageText: "Siemano", imageURL: "images/lewy.jpeg", time: "4 Cze"),
    ChatUsers(name: "Robert Lewandowski", messageText: "Siemano", imageURL: "images/lewy.jpeg", time: "4 Cze"),
    ChatUsers(name: "Robert Lewandowski", messageText: "Siemano", imageURL: "images/lewy.jpeg", time: "4 Cze"),
    ChatUsers(name: "Robert Lewandowski", messageText: "Siemano", imageURL: "images/lewy.jpeg", time: "4 Cze"),
    ChatUsers(name: "Robert Lewandowski", messageText: "Siemano", imageURL: "images/lewy.jpeg", time: "4 Cze"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SafeArea(
              child: Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Conversations",
                      style:
                          TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16, left: 16, right: 16),
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Search...",
                  hintStyle: TextStyle(color: Colors.grey.shade600),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.grey.shade600,
                    size: 20,
                  ),
                  filled: true,
                  fillColor: Colors.grey.shade100,
                  contentPadding: EdgeInsets.all(8),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: BorderSide(color: Colors.grey.shade100)),
                ),
              ),
            ),
            ListView.builder(
              itemCount: chatUsers.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 16),
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index){
                return ConversationList(
                  name: chatUsers[index].name,
                  messageText: chatUsers[index].messageText,
                  imageUrl: chatUsers[index].imageURL,
                  time: chatUsers[index].time,
                  isMessageRead: (index == 0 || index == 3)?true:false,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
