enum UserType { STUDENT, TEACHER, MENTOR, NONE }

class CommonUser {
  final String uid;
  final String? username;
  final String? email;
  final UserType? userType;

  CommonUser({
    required this.uid,
    this.username,
    this.email,
    this.userType,
  });

  factory CommonUser.fromJson(Map<String, dynamic> json) {
    return CommonUser(
      uid: json['uid'] as String,
      username: json['userName'] as String,
      email: json['email'] as String,
    );
  }

  Map<String, dynamic> toJson() => {
        'userName': this.username,
        'email': this.email,
      };

  @override
  String toString() {
    return 'CommonUser{uid: $uid, username: $username, email: $email, userType: $userType}';
  }
}
