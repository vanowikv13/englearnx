import 'package:flutter/material.dart';

import '../screens/model/common_user.dart';
import '../screens/chat_main_screen.dart';

class HomeScreen extends StatelessWidget {
  final CommonUser user;

  HomeScreen({
    required this.user,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'engLearnX',
      theme: ThemeData(
        primarySwatch: Colors.blue
      ),
      debugShowCheckedModeBanner: false,
      home: ChatMainScreen(),
    );
  }
}
