import 'package:firebase_auth/firebase_auth.dart';
import 'package:logger/logger.dart';
import 'package:login_app/screens/model/common_user.dart';

class AuthMethods {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Logger _logger = Logger();

  CommonUser? _userFromFirebaseUser(User? user) {
    return user != null ? CommonUser(uid: user.uid) : null;
  }

  Future<CommonUser?> signInWithEmailAndPassword(
      String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      return _userFromFirebaseUser(result.user);
    } catch (e) {
      _logger.e(e.toString());
    }
  }

  Future<CommonUser?> signUpWithEmailAndPassword(
      String email, String password) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      User? firebaseUser = result.user;
      return _userFromFirebaseUser(firebaseUser);
    } catch (e) {
      _logger.e(e.toString());
    }
  }

  Future<void> resetPassword(String email) async {
    try {
      return await _auth.sendPasswordResetEmail(email: email);
    } catch (e) {
      _logger.e(e.toString());
    }
  }

  Future<void> signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      _logger.e(e.toString());
    }
  }
}
