import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:logger/logger.dart';

class DatabaseMethods {
  Logger _logger = Logger();

  Future<QuerySnapshot> getUserByUserName(String username) async {
    QuerySnapshot snap = await FirebaseFirestore.instance
        .collection("users")
        .where(
          "name",
          isEqualTo: username,
        )
        .get();
    _logger.d(snap);

    return snap;
  }

  Future<QuerySnapshot> getUserByUserEmail(String userEmail) async {
    return await FirebaseFirestore.instance
        .collection("users")
        .where(
          "email",
          isEqualTo: userEmail,
        )
        .get();
  }

  Future<void> uploadUserInfo(Map<String, String> userMap) async {
    await FirebaseFirestore.instance
        .collection("users")
        .add(userMap)
        .catchError((e) {
      _logger.e(e.toString());
    });
  }
}
