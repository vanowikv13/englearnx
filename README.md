## EngLearnX


### Authors
* Yuliia Tarasenko
* Mariusz Nowicki


### How to run project
1. Install flutter from: https://flutter.dev/docs/get-started/install

2. Check if flutter is correctly installed and run in the terminal:
```
flutter doctor
```

3. Clone project:
```
git clone [repo_link]
```

4. Enter cloned repo:
```
cd engLearnX
```

5. Clean project possibly unimportant files:
```
flutter clean
```

6. Connect device or run emulator in android studio: https://developer.android.com/studio/run/managing-avds
